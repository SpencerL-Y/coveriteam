# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# Generated from CoVeriLang.g4 by ANTLR 4.8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2/")
        buf.write("\u01d2\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write('\t\36\4\37\t\37\4 \t \4!\t!\4"\t"\4#\t#\4$\t$\4%\t%')
        buf.write("\4&\t&\4'\t'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3")
        buf.write("\7\3\7\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n")
        buf.write("\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13")
        buf.write("\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f")
        buf.write("\3\f\3\f\3\f\3\f\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21")
        buf.write("\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22")
        buf.write("\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25")
        buf.write("\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27")
        buf.write("\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27")
        buf.write("\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\32")
        buf.write("\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33")
        buf.write("\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33")
        buf.write("\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\34")
        buf.write("\3\34\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3\36")
        buf.write("\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37")
        buf.write('\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \3 \3!\3!\3"')
        buf.write('\3"\3"\3"\3"\3"\3"\3"\3"\3"\3"\3"\3"\3"\3')
        buf.write('"\3"\3"\3"\3"\3"\3"\3"\5"\u0187\n"\3#\3#\3#')
        buf.write("\3#\3#\3#\3#\3#\3#\5#\u0192\n#\3$\3$\3$\3$\7$\u0198\n")
        buf.write("$\f$\16$\u019b\13$\3%\3%\7%\u019f\n%\f%\16%\u01a2\13%")
        buf.write("\3%\3%\3&\3&\3&\7&\u01a9\n&\f&\16&\u01ac\13&\3'\3'\5")
        buf.write("'\u01b0\n'\3(\3(\3)\3)\3*\3*\3+\5+\u01b9\n+\3+\3+\3")
        buf.write("+\3+\3,\6,\u01c0\n,\r,\16,\u01c1\3,\3,\3-\3-\3.\3.\3.")
        buf.write("\3.\7.\u01cc\n.\f.\16.\u01cf\13.\3.\3.\2\2/\3\3\5\4\7")
        buf.write("\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17")
        buf.write("\35\20\37\21!\22#\23%\24'\25)\26+\27-\30/\31\61\32\63")
        buf.write("\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K'M(O)Q*S+U,W-")
        buf.write("Y.[/\3\2\t\3\2aa\3\2$$\3\2c|\3\2C\\\3\2\62;\4\2\13\13")
        buf.write('""\4\2\f\f\17\17\2\u01e0\2\3\3\2\2\2\2\5\3\2\2\2\2\7')
        buf.write("\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2")
        buf.write("\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2")
        buf.write("\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2")
        buf.write("\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2'\3\2\2\2\2)\3\2\2")
        buf.write("\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63")
        buf.write("\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2")
        buf.write("\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2")
        buf.write("\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3")
        buf.write("\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y")
        buf.write("\3\2\2\2\2[\3\2\2\2\3]\3\2\2\2\5a\3\2\2\2\7c\3\2\2\2\t")
        buf.write("e\3\2\2\2\13g\3\2\2\2\ri\3\2\2\2\17k\3\2\2\2\21m\3\2\2")
        buf.write("\2\23t\3\2\2\2\25}\3\2\2\2\27\u0084\3\2\2\2\31\u0094\3")
        buf.write("\2\2\2\33\u0096\3\2\2\2\35\u00ab\3\2\2\2\37\u00b4\3\2")
        buf.write("\2\2!\u00b8\3\2\2\2#\u00bf\3\2\2\2%\u00c8\3\2\2\2'\u00cf")
        buf.write("\3\2\2\2)\u00da\3\2\2\2+\u00df\3\2\2\2-\u00e6\3\2\2\2")
        buf.write("/\u00f7\3\2\2\2\61\u0108\3\2\2\2\63\u012a\3\2\2\2\65\u0133")
        buf.write("\3\2\2\2\67\u014b\3\2\2\29\u014d\3\2\2\2;\u0151\3\2\2")
        buf.write("\2=\u015c\3\2\2\2?\u0166\3\2\2\2A\u016e\3\2\2\2C\u0186")
        buf.write("\3\2\2\2E\u0191\3\2\2\2G\u0193\3\2\2\2I\u019c\3\2\2\2")
        buf.write("K\u01a5\3\2\2\2M\u01af\3\2\2\2O\u01b1\3\2\2\2Q\u01b3\3")
        buf.write("\2\2\2S\u01b5\3\2\2\2U\u01b8\3\2\2\2W\u01bf\3\2\2\2Y\u01c5")
        buf.write("\3\2\2\2[\u01c7\3\2\2\2]^\7h\2\2^_\7w\2\2_`\7p\2\2`\4")
        buf.write("\3\2\2\2ab\7*\2\2b\6\3\2\2\2cd\7+\2\2d\b\3\2\2\2ef\7}")
        buf.write("\2\2f\n\3\2\2\2gh\7\177\2\2h\f\3\2\2\2ij\7.\2\2j\16\3")
        buf.write("\2\2\2kl\7?\2\2l\20\3\2\2\2mn\7r\2\2no\7t\2\2op\7k\2\2")
        buf.write("pq\7p\2\2qr\7v\2\2rs\7*\2\2s\22\3\2\2\2tu\7g\2\2uv\7z")
        buf.write("\2\2vw\7g\2\2wx\7e\2\2xy\7w\2\2yz\7v\2\2z{\7g\2\2{|\7")
        buf.write("*\2\2|\24\3\2\2\2}~\7t\2\2~\177\7g\2\2\177\u0080\7v\2")
        buf.write("\2\u0080\u0081\7w\2\2\u0081\u0082\7t\2\2\u0082\u0083\7")
        buf.write("p\2\2\u0083\26\3\2\2\2\u0084\u0085\7u\2\2\u0085\u0086")
        buf.write("\7g\2\2\u0086\u0087\7v\2\2\u0087\u0088\7a\2\2\u0088\u0089")
        buf.write("\7c\2\2\u0089\u008a\7e\2\2\u008a\u008b\7v\2\2\u008b\u008c")
        buf.write("\7q\2\2\u008c\u008d\7t\2\2\u008d\u008e\7a\2\2\u008e\u008f")
        buf.write("\7p\2\2\u008f\u0090\7c\2\2\u0090\u0091\7o\2\2\u0091\u0092")
        buf.write("\7g\2\2\u0092\u0093\7*\2\2\u0093\30\3\2\2\2\u0094\u0095")
        buf.write("\7<\2\2\u0095\32\3\2\2\2\u0096\u0097\7C\2\2\u0097\u0098")
        buf.write("\7e\2\2\u0098\u0099\7v\2\2\u0099\u009a\7q\2\2\u009a\u009b")
        buf.write("\7t\2\2\u009b\u009c\7H\2\2\u009c\u009d\7c\2\2\u009d\u009e")
        buf.write("\7e\2\2\u009e\u009f\7v\2\2\u009f\u00a0\7q\2\2\u00a0\u00a1")
        buf.write("\7t\2\2\u00a1\u00a2\7{\2\2\u00a2\u00a3\7\60\2\2\u00a3")
        buf.write("\u00a4\7e\2\2\u00a4\u00a5\7t\2\2\u00a5\u00a6\7g\2\2\u00a6")
        buf.write("\u00a7\7c\2\2\u00a7\u00a8\7v\2\2\u00a8\u00a9\7g\2\2\u00a9")
        buf.write("\u00aa\7*\2\2\u00aa\34\3\2\2\2\u00ab\u00ac\7U\2\2\u00ac")
        buf.write("\u00ad\7G\2\2\u00ad\u00ae\7S\2\2\u00ae\u00af\7W\2\2\u00af")
        buf.write("\u00b0\7G\2\2\u00b0\u00b1\7P\2\2\u00b1\u00b2\7E\2\2\u00b2")
        buf.write("\u00b3\7G\2\2\u00b3\36\3\2\2\2\u00b4\u00b5\7K\2\2\u00b5")
        buf.write("\u00b6\7V\2\2\u00b6\u00b7\7G\2\2\u00b7 \3\2\2\2\u00b8")
        buf.write("\u00b9\7T\2\2\u00b9\u00ba\7G\2\2\u00ba\u00bb\7R\2\2\u00bb")
        buf.write("\u00bc\7G\2\2\u00bc\u00bd\7C\2\2\u00bd\u00be\7V\2\2\u00be")
        buf.write('"\3\2\2\2\u00bf\u00c0\7R\2\2\u00c0\u00c1\7C\2\2\u00c1')
        buf.write("\u00c2\7T\2\2\u00c2\u00c3\7C\2\2\u00c3\u00c4\7N\2\2\u00c4")
        buf.write("\u00c5\7N\2\2\u00c5\u00c6\7G\2\2\u00c6\u00c7\7N\2\2\u00c7")
        buf.write("$\3\2\2\2\u00c8\u00c9\7L\2\2\u00c9\u00ca\7q\2\2\u00ca")
        buf.write("\u00cb\7k\2\2\u00cb\u00cc\7p\2\2\u00cc\u00cd\7g\2\2\u00cd")
        buf.write("\u00ce\7t\2\2\u00ce&\3\2\2\2\u00cf\u00d0\7E\2\2\u00d0")
        buf.write("\u00d1\7q\2\2\u00d1\u00d2\7o\2\2\u00d2\u00d3\7r\2\2\u00d3")
        buf.write("\u00d4\7c\2\2\u00d4\u00d5\7t\2\2\u00d5\u00d6\7c\2\2\u00d6")
        buf.write("\u00d7\7v\2\2\u00d7\u00d8\7q\2\2\u00d8\u00d9\7t\2\2\u00d9")
        buf.write("(\3\2\2\2\u00da\u00db\7E\2\2\u00db\u00dc\7q\2\2\u00dc")
        buf.write("\u00dd\7r\2\2\u00dd\u00de\7{\2\2\u00de*\3\2\2\2\u00df")
        buf.write("\u00e0\7T\2\2\u00e0\u00e1\7g\2\2\u00e1\u00e2\7p\2\2\u00e2")
        buf.write("\u00e3\7c\2\2\u00e3\u00e4\7o\2\2\u00e4\u00e5\7g\2\2\u00e5")
        buf.write(",\3\2\2\2\u00e6\u00e7\7V\2\2\u00e7\u00e8\7g\2\2\u00e8")
        buf.write("\u00e9\7u\2\2\u00e9\u00ea\7v\2\2\u00ea\u00eb\7U\2\2\u00eb")
        buf.write("\u00ec\7r\2\2\u00ec\u00ed\7g\2\2\u00ed\u00ee\7e\2\2\u00ee")
        buf.write("\u00ef\7V\2\2\u00ef\u00f0\7q\2\2\u00f0\u00f1\7U\2\2\u00f1")
        buf.write("\u00f2\7r\2\2\u00f2\u00f3\7g\2\2\u00f3\u00f4\7e\2\2\u00f4")
        buf.write("\u00f5\7*\2\2\u00f5\u00f6\7+\2\2\u00f6.\3\2\2\2\u00f7")
        buf.write("\u00f8\7U\2\2\u00f8\u00f9\7r\2\2\u00f9\u00fa\7g\2\2\u00fa")
        buf.write("\u00fb\7e\2\2\u00fb\u00fc\7V\2\2\u00fc\u00fd\7q\2\2\u00fd")
        buf.write("\u00fe\7V\2\2\u00fe\u00ff\7g\2\2\u00ff\u0100\7u\2\2\u0100")
        buf.write("\u0101\7v\2\2\u0101\u0102\7U\2\2\u0102\u0103\7r\2\2\u0103")
        buf.write("\u0104\7g\2\2\u0104\u0105\7e\2\2\u0105\u0106\7*\2\2\u0106")
        buf.write("\u0107\7+\2\2\u0107\60\3\2\2\2\u0108\u0109\7E\2\2\u0109")
        buf.write("\u010a\7n\2\2\u010a\u010b\7c\2\2\u010b\u010c\7u\2\2\u010c")
        buf.write("\u010d\7u\2\2\u010d\u010e\7k\2\2\u010e\u010f\7h\2\2\u010f")
        buf.write("\u0110\7k\2\2\u0110\u0111\7e\2\2\u0111\u0112\7c\2\2\u0112")
        buf.write("\u0113\7v\2\2\u0113\u0114\7k\2\2\u0114\u0115\7q\2\2\u0115")
        buf.write("\u0116\7p\2\2\u0116\u0117\7V\2\2\u0117\u0118\7q\2\2\u0118")
        buf.write("\u0119\7C\2\2\u0119\u011a\7e\2\2\u011a\u011b\7v\2\2\u011b")
        buf.write("\u011c\7q\2\2\u011c\u011d\7t\2\2\u011d\u011e\7F\2\2\u011e")
        buf.write("\u011f\7g\2\2\u011f\u0120\7h\2\2\u0120\u0121\7k\2\2\u0121")
        buf.write("\u0122\7p\2\2\u0122\u0123\7k\2\2\u0123\u0124\7v\2\2\u0124")
        buf.write("\u0125\7k\2\2\u0125\u0126\7q\2\2\u0126\u0127\7p\2\2\u0127")
        buf.write("\u0128\7*\2\2\u0128\u0129\7+\2\2\u0129\62\3\2\2\2\u012a")
        buf.write("\u012b\7K\2\2\u012b\u012c\7f\2\2\u012c\u012d\7g\2\2\u012d")
        buf.write("\u012e\7p\2\2\u012e\u012f\7v\2\2\u012f\u0130\7k\2\2\u0130")
        buf.write("\u0131\7v\2\2\u0131\u0132\7{\2\2\u0132\64\3\2\2\2\u0133")
        buf.write("\u0134\7C\2\2\u0134\u0135\7t\2\2\u0135\u0136\7v\2\2\u0136")
        buf.write("\u0137\7k\2\2\u0137\u0138\7h\2\2\u0138\u0139\7c\2\2\u0139")
        buf.write("\u013a\7e\2\2\u013a\u013b\7v\2\2\u013b\u013c\7H\2\2\u013c")
        buf.write("\u013d\7c\2\2\u013d\u013e\7e\2\2\u013e\u013f\7v\2\2\u013f")
        buf.write("\u0140\7q\2\2\u0140\u0141\7t\2\2\u0141\u0142\7{\2\2\u0142")
        buf.write("\u0143\7\60\2\2\u0143\u0144\7e\2\2\u0144\u0145\7t\2\2")
        buf.write("\u0145\u0146\7g\2\2\u0146\u0147\7c\2\2\u0147\u0148\7v")
        buf.write("\2\2\u0148\u0149\7g\2\2\u0149\u014a\7*\2\2\u014a\66\3")
        buf.write("\2\2\2\u014b\u014c\7\60\2\2\u014c8\3\2\2\2\u014d\u014e")
        buf.write("\7P\2\2\u014e\u014f\7Q\2\2\u014f\u0150\7V\2\2\u0150:\3")
        buf.write("\2\2\2\u0151\u0152\7K\2\2\u0152\u0153\7P\2\2\u0153\u0154")
        buf.write("\7U\2\2\u0154\u0155\7V\2\2\u0155\u0156\7C\2\2\u0156\u0157")
        buf.write("\7P\2\2\u0157\u0158\7E\2\2\u0158\u0159\7G\2\2\u0159\u015a")
        buf.write("\7Q\2\2\u015a\u015b\7H\2\2\u015b<\3\2\2\2\u015c\u015d")
        buf.write("\7G\2\2\u015d\u015e\7N\2\2\u015e\u015f\7G\2\2\u015f\u0160")
        buf.write("\7O\2\2\u0160\u0161\7G\2\2\u0161\u0162\7P\2\2\u0162\u0163")
        buf.write("\7V\2\2\u0163\u0164\7Q\2\2\u0164\u0165\7H\2\2\u0165>\3")
        buf.write("\2\2\2\u0166\u0167\7V\2\2\u0167\u0168\7Q\2\2\u0168\u0169")
        buf.write("\7F\2\2\u0169\u016a\7Q\2\2\u016a\u016b\7/\2\2\u016b\u016c")
        buf.write("\7/\2\2\u016c\u016d\7/\2\2\u016d@\3\2\2\2\u016e\u016f")
        buf.write("\7)\2\2\u016fB\3\2\2\2\u0170\u0171\7H\2\2\u0171\u0172")
        buf.write("\7C\2\2\u0172\u0173\7N\2\2\u0173\u0174\7U\2\2\u0174\u0175")
        buf.write('\7G\2\2\u0175\u0176\3\2\2\2\u0176\u0187\b"\2\2\u0177')
        buf.write("\u0178\7V\2\2\u0178\u0179\7T\2\2\u0179\u017a\7W\2\2\u017a")
        buf.write('\u017b\7G\2\2\u017b\u017c\3\2\2\2\u017c\u0187\b"\3\2')
        buf.write("\u017d\u017e\7W\2\2\u017e\u017f\7P\2\2\u017f\u0180\7M")
        buf.write("\2\2\u0180\u0181\7P\2\2\u0181\u0182\7Q\2\2\u0182\u0183")
        buf.write("\7Y\2\2\u0183\u0184\7P\2\2\u0184\u0185\3\2\2\2\u0185\u0187")
        buf.write('\b"\4\2\u0186\u0170\3\2\2\2\u0186\u0177\3\2\2\2\u0186')
        buf.write("\u017d\3\2\2\2\u0187D\3\2\2\2\u0188\u0189\7C\2\2\u0189")
        buf.write("\u018a\7P\2\2\u018a\u0192\7F\2\2\u018b\u018c\7Q\2\2\u018c")
        buf.write("\u0192\7T\2\2\u018d\u018e\7?\2\2\u018e\u0192\7?\2\2\u018f")
        buf.write("\u0190\7#\2\2\u0190\u0192\7?\2\2\u0191\u0188\3\2\2\2\u0191")
        buf.write("\u018b\3\2\2\2\u0191\u018d\3\2\2\2\u0191\u018f\3\2\2\2")
        buf.write("\u0192F\3\2\2\2\u0193\u0199\5O(\2\u0194\u0198\5M'\2\u0195")
        buf.write("\u0198\5S*\2\u0196\u0198\t\2\2\2\u0197\u0194\3\2\2\2\u0197")
        buf.write("\u0195\3\2\2\2\u0197\u0196\3\2\2\2\u0198\u019b\3\2\2\2")
        buf.write("\u0199\u0197\3\2\2\2\u0199\u019a\3\2\2\2\u019aH\3\2\2")
        buf.write("\2\u019b\u0199\3\2\2\2\u019c\u01a0\7$\2\2\u019d\u019f")
        buf.write("\n\3\2\2\u019e\u019d\3\2\2\2\u019f\u01a2\3\2\2\2\u01a0")
        buf.write("\u019e\3\2\2\2\u01a0\u01a1\3\2\2\2\u01a1\u01a3\3\2\2\2")
        buf.write("\u01a2\u01a0\3\2\2\2\u01a3\u01a4\7$\2\2\u01a4J\3\2\2\2")
        buf.write("\u01a5\u01aa\5Q)\2\u01a6\u01a9\5M'\2\u01a7\u01a9\5S*")
        buf.write("\2\u01a8\u01a6\3\2\2\2\u01a8\u01a7\3\2\2\2\u01a9\u01ac")
        buf.write("\3\2\2\2\u01aa\u01a8\3\2\2\2\u01aa\u01ab\3\2\2\2\u01ab")
        buf.write("L\3\2\2\2\u01ac\u01aa\3\2\2\2\u01ad\u01b0\5O(\2\u01ae")
        buf.write("\u01b0\5Q)\2\u01af\u01ad\3\2\2\2\u01af\u01ae\3\2\2\2\u01b0")
        buf.write("N\3\2\2\2\u01b1\u01b2\t\4\2\2\u01b2P\3\2\2\2\u01b3\u01b4")
        buf.write("\t\5\2\2\u01b4R\3\2\2\2\u01b5\u01b6\t\6\2\2\u01b6T\3\2")
        buf.write("\2\2\u01b7\u01b9\7\17\2\2\u01b8\u01b7\3\2\2\2\u01b8\u01b9")
        buf.write("\3\2\2\2\u01b9\u01ba\3\2\2\2\u01ba\u01bb\7\f\2\2\u01bb")
        buf.write("\u01bc\3\2\2\2\u01bc\u01bd\b+\5\2\u01bdV\3\2\2\2\u01be")
        buf.write("\u01c0\t\7\2\2\u01bf\u01be\3\2\2\2\u01c0\u01c1\3\2\2\2")
        buf.write("\u01c1\u01bf\3\2\2\2\u01c1\u01c2\3\2\2\2\u01c2\u01c3\3")
        buf.write("\2\2\2\u01c3\u01c4\b,\5\2\u01c4X\3\2\2\2\u01c5\u01c6\7")
        buf.write("=\2\2\u01c6Z\3\2\2\2\u01c7\u01c8\7\61\2\2\u01c8\u01c9")
        buf.write("\7\61\2\2\u01c9\u01cd\3\2\2\2\u01ca\u01cc\n\b\2\2\u01cb")
        buf.write("\u01ca\3\2\2\2\u01cc\u01cf\3\2\2\2\u01cd\u01cb\3\2\2\2")
        buf.write("\u01cd\u01ce\3\2\2\2\u01ce\u01d0\3\2\2\2\u01cf\u01cd\3")
        buf.write("\2\2\2\u01d0\u01d1\b.\5\2\u01d1\\\3\2\2\2\16\2\u0186\u0191")
        buf.write("\u0197\u0199\u01a0\u01a8\u01aa\u01af\u01b8\u01c1\u01cd")
        buf.write('\6\3"\2\3"\3\3"\4\b\2\2')
        return buf.getvalue()


class CoVeriLangLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [DFA(ds, i) for i, ds in enumerate(atn.decisionToState)]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    T__21 = 22
    T__22 = 23
    T__23 = 24
    T__24 = 25
    T__25 = 26
    T__26 = 27
    T__27 = 28
    T__28 = 29
    T__29 = 30
    T__30 = 31
    T__31 = 32
    VERDICT = 33
    BIN_OP = 34
    ID = 35
    STRING = 36
    TYPE_NAME = 37
    LETTER = 38
    LOWER_CASE = 39
    UPPER_CASE = 40
    DIGIT = 41
    NEWLINE = 42
    WS = 43
    DELIMITER = 44
    COMMENT = 45

    channelNames = [u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN"]

    modeNames = ["DEFAULT_MODE"]

    literalNames = [
        "<INVALID>",
        "'fun'",
        "'('",
        "')'",
        "'{'",
        "'}'",
        "','",
        "'='",
        "'print('",
        "'execute('",
        "'return'",
        "'set_actor_name('",
        "':'",
        "'ActorFactory.create('",
        "'SEQUENCE'",
        "'ITE'",
        "'REPEAT'",
        "'PARALLEL'",
        "'Joiner'",
        "'Comparator'",
        "'Copy'",
        "'Rename'",
        "'TestSpecToSpec()'",
        "'SpecToTestSpec()'",
        "'ClassificationToActorDefinition()'",
        "'Identity'",
        "'ArtifactFactory.create('",
        "'.'",
        "'NOT'",
        "'INSTANCEOF'",
        "'ELEMENTOF'",
        "'TODO---'",
        "'''",
        "';'",
    ]

    symbolicNames = [
        "<INVALID>",
        "VERDICT",
        "BIN_OP",
        "ID",
        "STRING",
        "TYPE_NAME",
        "LETTER",
        "LOWER_CASE",
        "UPPER_CASE",
        "DIGIT",
        "NEWLINE",
        "WS",
        "DELIMITER",
        "COMMENT",
    ]

    ruleNames = [
        "T__0",
        "T__1",
        "T__2",
        "T__3",
        "T__4",
        "T__5",
        "T__6",
        "T__7",
        "T__8",
        "T__9",
        "T__10",
        "T__11",
        "T__12",
        "T__13",
        "T__14",
        "T__15",
        "T__16",
        "T__17",
        "T__18",
        "T__19",
        "T__20",
        "T__21",
        "T__22",
        "T__23",
        "T__24",
        "T__25",
        "T__26",
        "T__27",
        "T__28",
        "T__29",
        "T__30",
        "T__31",
        "VERDICT",
        "BIN_OP",
        "ID",
        "STRING",
        "TYPE_NAME",
        "LETTER",
        "LOWER_CASE",
        "UPPER_CASE",
        "DIGIT",
        "NEWLINE",
        "WS",
        "DELIMITER",
        "COMMENT",
    ]

    grammarFileName = "CoVeriLang.g4"

    def __init__(self, input=None, output: TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(
            self, self.atn, self.decisionsToDFA, PredictionContextCache()
        )
        self._actions = None
        self._predicates = None

    def action(self, localctx: RuleContext, ruleIndex: int, actionIndex: int):
        if self._actions is None:
            actions = dict()
            actions[32] = self.VERDICT_action
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))

    def VERDICT_action(self, localctx: RuleContext, actionIndex: int):
        if actionIndex == 0:
            self.text = "RESULT_CLASS_FALSE"

        if actionIndex == 1:
            self.text = "RESULT_CLASS_TRUE"

        if actionIndex == 2:
            self.text = "RESULT_CLASS_OTHER"
