# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# To execute a job locally, install gitlab-runner (https://docs.gitlab.com/runner/install/)
# and run the following command:
# gitlab-runner exec docker --docker-privileged --docker-volumes /sys/fs/cgroup:/sys/fs/cgroup:rw --env CI_REGISTRY_IMAGE=registry.gitlab.com/sosy-lab/software/coveriteam <<JOB_NAME>>


variables:
  PRIMARY_USER: sosyuser
  XDG_CACHE_HOME: /home/${PRIMARY_USER}/.cache

image: ${CI_REGISTRY_IMAGE}/test:python-${PYTHON_VERSION}

stages:
  - images
  - test

.unit-tests: &unit-tests
  stage: test
  before_script:
    # Create user, we do not want to test as root
    - adduser --disabled-login --gecos "" $PRIMARY_USER
    # Give $PRIMARY_USER permission to create cgroups
    - test/for_each_of_my_cgroups.sh chgrp $PRIMARY_USER
    - test/for_each_of_my_cgroups.sh chmod g+w
  script:
    # verbose temporarily till the tests stabilize
    - sudo -u $PRIMARY_USER XDG_CACHE_HOME=${XDG_CACHE_HOME} nosetests --verbose --with-coverage --cover-package=coveriteam test/
  tags:
    - privileged
  only:
    variables:
      - $CI_COMMIT_REF_PROTECTED

unit-tests:python-3.6:
  <<: *unit-tests
  variables:
    PYTHON_VERSION: '3.6'

unit-tests:python-3.8:
  <<: *unit-tests
  variables:
    PYTHON_VERSION: '3.8'

# Static checks
check-format:
  stage: test
  image: python:3.6
  before_script:
    - pip install black
  script:
    - black . --check --diff

flake8:
  stage: test
  image: python:3.6
  before_script:
    - pip install 'isort<5' # workaround for https://github.com/gforcada/flake8-isort/issues/88
    - pip install flake8-awesome
  script:
    - flake8

# check license declarations etc.
reuse:
  stage: test
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint

# Build Docker images
# following this guideline: https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
.build-docker:
  stage: images
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /root/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --dockerfile $CI_PROJECT_DIR/$DOCKERFILE --destination $CI_REGISTRY_IMAGE/$IMAGE
  only:
    - schedules
    - web

build-docker:test:python-3.6:
  extends: .build-docker
  variables:
    DOCKERFILE: test/Dockerfile.python-3.6
    IMAGE: test:python-3.6
  only:
    changes:
      - "test/Dockerfile.python-3.6"

build-docker:test:python-3.8:
  extends: .build-docker
  variables:
    DOCKERFILE: test/Dockerfile.python-3.8
    IMAGE: test:python-3.8
  only:
    changes:
      - "test/Dockerfile.python-3.8"

